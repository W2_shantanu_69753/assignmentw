// Update the content of src/App.tsx
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Line } from 'react-chartjs-2';

const App: React.FC = () => {
  const [data, setData] = useState<any[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get('/api/data');
        setData(response.data);
      } catch (error) {
        console.log(error);
      }
    };

    fetchData();
  }, []);

  // Process data to fit the chart's format
  const chartData = {
    labels: data.map((entry) => entry.timestamp),
    datasets: [
      {
        label: 'Sample Data',
        data: data.map((entry) => entry.value),
        borderColor: 'rgba(75,192,192,1)',
        backgroundColor: 'rgba(75,192,192,0.4)',
      },
    ],
  };

  return (
    <div>
      <h1>Real-time Data Chart</h1>
      <Line data={chartData} />
    </div>
  );
};

export default App;
